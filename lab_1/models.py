from django.db import models
from datetime import datetime, date

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    birth_date = models.DateField()
    npm = models.CharField(max_length=10)
    # TODO Implement missing attributes in Friend model
