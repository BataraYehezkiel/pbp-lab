1. Apakah perbedaan antara JSON dan XML?

- Berdasarkan bahasa, XML (Extensible Markup Language) menggunakan bahasa markup, bukan bahasa pemrograman, yang memiliki tag untuk mendefinisikan elemen. XML hanyalah informasi yang dibungkus di dalam tag. Kita perlu menulis program untuk mengirim, menerima, menyimpan, atau menampilkan informasi tersebut. Sedangkan JSON (JavaScript Object Notation) hanyalah format yang ditulis dalam JavaScript. Sintaks JSON merupakan turunan dari Object JavaScript. Akan tetapi format JSON berbentuk text, sehingga kode untuk membaca dan membuat JSON banyak terdapat di banyak bahasa pemrograman.

- XML didesain menjadi self-descriptive, jadi dengan membaca XML tersebut kita dapat mengerti informasi apa yang ingin disampaikan dari data yang tertulis. JSON didesain menjadi self-describing, sehingga JSON sangat mudah dan tentunya lebih mudah dari XML untuk dimengerti.

- Data dalam XML disimpan sebagai struktur seperti tree yang dimulai dari root, lalu branch, hingga berakhir pada leaves. Sedangkan JSON, data disimpan seperti map dengan pasangan key dan value.

- JSON juga menghasilkan objek yang bertipe sedangkan XML datanya tidak bertipe. JSON memiliki tipe string, number, array, dan boolean. Untuk XML semua data disimpan sebagai string saja.

- Menurut berbagai sumber, JSON dalam hal sekuritas lebih rendah daripada XML. Namun setelah saya memahami lebih lanjut keduanya, JSON dan XML, hanyalah metode untuk merepresentasikan data terstruktur. Jadi tidak satupun dari mereka dapat dibandingkan sebagai lebih aman.

- XML tidak mendukung array sedangkan JSON mendukung array yang dapat diakses

2. Apakah perbedaan antara HTML dan XML?

- Untuk bahasa, XML merupakan bahasa markup yang dapat diperluas dan dikembangkan. Bahasa yang digunakan pada HTML merupakan bahasa yang sederhana serta mendukung pembuatan halaman website.

- Walaupun dengan XML ataupun JSON, kita dapat mengerti informasi apa yang ingin disampaikan tetapi untuk penyajian yang lebih baik HTML adalah jawabannya. XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.

- XML juga secara umum dapat menghasilkan output yang dynamic sedangkan HTML hanya dapat menampilkan halaman yang static secara umum.

- Tag pada XML sifatnya case sensitive sedangkan HTML tidak case sensitive.

- XML strict untuk tag penutup sedangkan HTML tidak strict.

Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.upgrad.com/blog/html-vs-xml/