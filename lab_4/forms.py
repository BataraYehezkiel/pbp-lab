# import form class from django
from django import forms
from lab_2.models import Note
  
# create a ModelForm
class NoteForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Note
        fields = "__all__"