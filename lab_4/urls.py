from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'), # http://localhost:8000/lab-4
    path('add-note', add_note, name='add-note'), # http://localhost:8000/lab-4/add-note
    path('note-list', note_list, name='note-list') # http://localhost:8000/lab-4/note-list
]