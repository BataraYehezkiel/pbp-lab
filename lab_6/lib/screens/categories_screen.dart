import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/profile_item.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_PROFILE
          .map(
            (catData) => ProfileItem(
                  catData.username,
                  catData.email,
                  catData.phone,
                  catData.color,
                ),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 700,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
        mainAxisExtent: 350,
      ),
    );
  }
}
