import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../screens/category_meals_screen.dart';

class ProfileItem extends StatelessWidget {
  final String username;
  final String email;
  final String phone;
  final Color color;

  ProfileItem(this.username, this.email, this.phone, this.color);

//   void selectCategory(BuildContext ctx) {
//     Navigator.of(ctx).pushNamed(
//       CategoryMealsScreen.routeName,
//       arguments: {
//         'id': id,
//         'title': title,
//       },
//     );
//   }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: 
      Container(
        child: Wrap(
          //sumber : https://medium.com/@palmeiro.leonardo/simple-profile-screen-with-flutter-fe2f1f7cfaf5
          children: <Widget>[
            CircleAvatar(
              backgroundColor: Colors.white70,
              minRadius: 55.0,
              child: CircleAvatar(
                radius: 50.0,
                backgroundImage: NetworkImage(
                    'https://avatars0.githubusercontent.com/u/28812093?s=460&u=06471c90e03cfd8ce2855d217d157c93060da490&v=4'),
              ),
            ),
            ListTile(
              title: Text(
                'Username',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                username,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Divider(),
            ListTile(
              title: Text(
                'Email',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                email,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Divider(),
            ListTile(
              title: Text(
                'Phone',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                phone,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
