import 'package:flutter/material.dart';

class Profile {
  final String username;
  final String email;
  final String phone;
  final Color color;

  const Profile(
    {@required this.username,
    @required this.email,
    @required this.phone,
    this.color = const Color(0xFFBA68C8), 
    });
}