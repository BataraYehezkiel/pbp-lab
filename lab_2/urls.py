from django.urls import path
from .views import index, json, xml

urlpatterns = [
    path('', index, name='index'), # http://localhost:8000/lab-2
    path('xml', xml, name='xml'), # http://localhost:8000/lab-2/xml
    path('json', json, name='json'), # http://localhost:8000/lab-2/json
]