from django.urls import path
from .views import add_friend, index

urlpatterns = [
    path('', index, name='index'), #http://localhost:8000/lab-3
    path('add', add_friend, name='add_friend') 
]
